package models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;


public class Offre {
	/* ATTRIBUTES */
	
	private int idOffre;
	private int nbrLits;
	private String dateArrivee;
	private String dateDepart;
	private float prix;
	
	private byte[] imgEncode;
	
	/* CONSTRUCTORS */
	public Offre() {
		
	}
	public Offre(int idOffre, int nbrLits, String dateArrivee, String dateDepart, float prix,byte[] imgEncode) {
		super();
		this.idOffre = idOffre;
		this.nbrLits = nbrLits;
		this.dateArrivee = dateArrivee;
		this.dateDepart = dateDepart;
		this.prix = prix;
		this.imgEncode=imgEncode;
	}
	
	/* METHODS */
	public int getIdOffre() {
		return idOffre;
	}
	public void setIdOffre(int idOffre) {
		this.idOffre = idOffre;
	}
	public int getNbrLits() {
		return nbrLits;
	}
	public void setNbrLits(int nbrLit) {
		this.nbrLits = nbrLit;
	}
	public String getDateArrivee() {
		return dateArrivee;
	}
	public void setDateArrivee(String dateArrivee) {
		this.dateArrivee = dateArrivee;
	}
	public String getDateDepart() {
		return dateDepart;
	}
	public void setDateDepart(String dateDepart) {
		this.dateDepart = dateDepart;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	public byte[] getImgEncode() {
		return imgEncode;
	}
	public void setImgEncode(byte[] imgChambre) {
		this.imgEncode = imgChambre;
	}
	
	

}
