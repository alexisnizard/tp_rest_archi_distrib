package models;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class InfosHotel {
	
	/* ATTRIBUTES */
	@Id
	private String nomHotel;
	
	private String villeHotel;
	private String urlWS;
	private String mdpWS;
	private float percentAdd;

	private int nbrEtoiles;
	private String pays;
	private String rue;
	

	/* CONSTRUCTORS */
	public InfosHotel() {
		
	}
	
	public InfosHotel(String nomHotel, String villeHotel, String urlWS, String mdpWS, float percentAdd, int nbrEtoiles,
			String pays, String rue) {
		super();
		this.nomHotel = nomHotel;
		this.villeHotel = villeHotel;
		this.urlWS = urlWS;
		this.mdpWS = mdpWS;
		this.percentAdd = percentAdd;
		this.nbrEtoiles = nbrEtoiles;
		this.pays = pays;
		this.rue = rue;
	}
	/* METHODS*/
	public String getNomHotel() {
		return nomHotel;
	}
	public void setNomHotel(String nomHotel) {
		this.nomHotel = nomHotel;
	}
	public String getVilleHotel() {
		return villeHotel;
	}
	public void setVilleHotel(String villeHotel) {
		this.villeHotel = villeHotel;
	}
	public String getMdpWS() {
		return mdpWS;
	}
	public void setMdpWS(String mdpWS) {
		this.mdpWS = mdpWS;
	}
	public float getPercentAdd() {
		return percentAdd;
	}
	public void setPercentAdd(float percentAdd) {
		this.percentAdd = percentAdd;
	}
	public String getUrlWS() {
		return urlWS;
	}
	public void setUrlWS(String urlWS) {
		this.urlWS = urlWS;
	}
	public int getNbrEtoiles() {
		return nbrEtoiles;
	}
	public void setNbrEtoiles(int nbrEtoiles) {
		this.nbrEtoiles = nbrEtoiles;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}


	
	
}