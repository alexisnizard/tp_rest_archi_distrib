package repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import models.InfosHotel;

public interface InfosHotelRepository extends JpaRepository<InfosHotel, String>{
}
