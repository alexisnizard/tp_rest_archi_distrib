# Agence de voyage et hôtels - API REST avec Spring Boot

## Sujet
L’objectif de ce TP est de développer en Java en utilisant **des services web REST** une application de réservation d’hôtels en ligne.
Cette application propose une interface permettant à un utilisateur de saisir les informations suivantes : Une ville de séjour, une date d’arrivée, une date de départ, un intervalle de prix souhaité, une catégorie d’hôtel : nombre
d’étoiles, le nombre de personnes à héberger.

En réponse, l’application lui retourne une liste d’hôtels qui répondent à ses critères et où pour chaque hôtel les informations suivantes sont affichées : nom de l’hôtel, adresse de l’hôtel (pays, ville, rue, numéro, lieu-dit, position GPS), le prix à payer, nombre d’étoile, nombre de lits proposés.

L’utilisateur choisira un hôtel dans la liste proposée et l’application lui demandera les informations suivantes : le nom et prénom de la personne principale à héberger, les informations de la carte de crédit de paiement. L’application utilisera ces informations pour réaliser la réservation de l’hôtel en question. .



## Tester la version distribuée
Il est possible de tester notre application depuis un IDE en :

\> important les 6 projets maven suivant :

- versionDistribueHotelsREST
- versionDistribueHotels 02 REST
- versionDistribueHotels 03 REST
- versionDistribueAgence 01 REST
- versionDistribueAgence 02 REST
- versionDistribueComparateur TRIVALGO REST

\> lançant d’abord les 3 serveurs un à un (veuillez lancer le programme StartServeursHotelsApplication
à l’intérieur du package main de chaque serveur)

\> puis en lançant la CLI d’une Agence (veuillez lancer le programme AgenceApplication à l’intérieur
du package main de l’Agence)

Pour tester le comparateur TRIVALGO, veuillez vous assurer d’avoir lancer les 3 serveurs des hôtels
ET la CLI des deux Agences, puis veuillez lancer le programme ComparateurTrivalgoApplication
disponible à l’intérieur du package main du projet versionDistribueComparateur TRIVALGO REST.

## UML du projet

<p align="center">
  <img src="UML.png" alt="uml" width="800">
</p>

## Exemple d'affichage

Ci-dessous, vous trouverez un exemple d'affichage du programme ainsi qu'une recherche sur Postman :

<p align="center">
  <img src="exemple1.png" alt="Exemple d'exécution du programme" width="800">
  <br>
  <img src="exemple2.png" alt="Exemple d'exécution du programme" width="800">
</p>
