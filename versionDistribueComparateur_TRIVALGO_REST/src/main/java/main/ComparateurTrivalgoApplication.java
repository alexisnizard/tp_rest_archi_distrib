package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = {
		"models"
})
@SpringBootApplication(scanBasePackages = {
		"cli"
})
public class ComparateurTrivalgoApplication {
	public static void main(String[] args) {
		SpringApplication.run(ComparateurTrivalgoApplication.class, args);
	}
}
