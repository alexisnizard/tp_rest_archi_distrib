package cli;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


import models.ConvertLocalDateTime;
import models.OffreWS;
import models.RechercheWS_DTO;


@Component
public class CLI implements CommandLineRunner{	
	List<String> nomsDesAgences = new ArrayList<>();
	List<String> uriAgencesPartenaires = new ArrayList<>();
	
	@Autowired
	private RestTemplate proxy;
	
	public static String enleverLaSensiCasse(String s) {
		return s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase();
	}
		
	@Override
	public void run(String... args) throws Exception {
		
		nomsDesAgences.add("Agence1");
		nomsDesAgences.add("Agence2");
		uriAgencesPartenaires.add("http://localhost:7900/agence/api/recherche");
		uriAgencesPartenaires.add("http://localhost:7901/agence/api/recherche");		

		Scanner sc= new Scanner(System.in);
		Regex rg= new Regex();
		String choix;
		ConvertLocalDateTime pDT = new ConvertLocalDateTime(); //Utiliser pour transformer un String en LocalDateTime et inversement
				
		System.out.println("Bienvenue ! ");

		while (true) {
			System.out.println("Veuillez sélectionner une option ! ");
			System.out.println("1 - Rechercher une chambre d'hôtel à l'aide de notre comparateur ");
			System.out.println("0 - Exit");

			choix=sc.nextLine();
			switch(choix) {
			case "1":

				/* CHOIX VILLE SEJOUR*/
				System.out.println("Veuillez sélectionner la ville de votre séjour");

				String choixVille;
				choixVille=sc.nextLine();
				while (!rg.estAlphabetique(choixVille)) {           
					System.err.println("Mauvaise input /!\\ Veuillez entrer un nom de ville correct ");
					choixVille = sc.nextLine();
				}

				/* CHOIX DATE ARRIVEE*/
				System.out.println("Veuillez sélectionner votre date d'arrivée au format JJ/MM/AAAA HH:MM");

				String choixDateArrive;
				choixDateArrive=sc.nextLine();
				while (!rg.estUneDate(choixDateArrive)) {
					System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
					choixDateArrive=sc.nextLine();
				}

				String[] separeDateHeure= choixDateArrive.split(" ");
				String[] jjMMaaaa= separeDateHeure[0].split("/");
				String[] hhMM= separeDateHeure[1].split(":");
				LocalDateTime dateArrivee=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
						,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));

				/* CHOIX DATE DEPART*/
				System.out.println("Veuillez sélectionner votre date de départ au format JJ/MM/AAAA HH:MM");

				String choixDateDepart;
				choixDateDepart=sc.nextLine();
				while (!rg.estUneDate(choixDateDepart)) {
					System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
					choixDateDepart=sc.nextLine();
				}

				separeDateHeure= choixDateDepart.split(" ");
				jjMMaaaa= separeDateHeure[0].split("/");
				hhMM= separeDateHeure[1].split(":");
				LocalDateTime dateDepart=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
						,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));

				while(dateDepart.isBefore(dateArrivee)) {             
					System.err.println("Mauvaise input /!\\ Veuillez entrer une date de départ supérieur à votre date d'arrivée");

					choixDateDepart=sc.nextLine();
					while (!rg.estUneDate(choixDateDepart)) {
						System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
						choixDateDepart=sc.nextLine();
					}

					separeDateHeure= choixDateDepart.split(" ");
					jjMMaaaa= separeDateHeure[0].split("/");
					hhMM= separeDateHeure[1].split(":");
					dateDepart=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
							,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));
				} 

				/* CHOIX NBR PERSONNE A HEBERGER*/
				System.out.println("Veuillez sélectionner le nombre de personne à héberger dans la chambre");
				int nbrPers;
				while (true) {
					try{
						nbrPers= sc.nextInt();
						if (nbrPers<1 || nbrPers>6) {
							throw new IllegalArgumentException();               
						}
						break;                
					}catch(Exception e) {
						System.err.println("Mauvaise input /!\\ Veuillez entrer un nombre de personne positif "
								+ "et réaliste pour une seule chambre");
						sc.nextLine();
					}
				}
				
				/* CHOIX TYPE D'HOTEL*/
				System.out.println("Veuillez sélectionner le type de l'hôtel (nombre MINIMUM d'étoiles de l'hôtel)");
				int nbrEtoi;
				while (true) {
					try{
						nbrEtoi= sc.nextInt();
						if (nbrEtoi<1 || nbrEtoi>5) {
							throw new IllegalArgumentException();               
						}
						break;                
					}catch(Exception e) {
						System.err.println("Mauvaise input /!\\ Veuillez entrer un nombre d'étoile positif "
								+ "et inférieur ou égal à 5");
						sc.nextLine();
					}
				}

				/* CONSULTATION DES OFFRES AUPRES DES AGENCES PARTENAIRES  */
				int aumoinsuneoffretrouve=0;
				List<OffreWS[]> listetouteslesOffres = new ArrayList<>();
				for (String uri : uriAgencesPartenaires) {
					
					RechercheWS_DTO recherche = new RechercheWS_DTO(choixVille,pDT.DateToString(dateArrivee),pDT.DateToString(dateDepart),nbrPers,nbrEtoi);
					OffreWS[] listeOffres = proxy.postForObject(uri,recherche,OffreWS[].class);
					
					listetouteslesOffres.add(listeOffres);

					if (listeOffres != null) {					
						aumoinsuneoffretrouve++;
					}
				}
				
				if (aumoinsuneoffretrouve==0) {
					System.out.println("Aucune offre correspondant à vos critères n'a été trouvé par nos agences partenaires.");
					System.out.println("Merci et à bientôt !");
					System.exit(0);
					return;
				}
				
				
				System.out.println("\n======================= RESULTATS TROUVES ===============================\n");
				
				
				for (int i =0;i<nomsDesAgences.size();i++) {
					if (listetouteslesOffres.get(i)!=null) {
						System.out.println(nomsDesAgences.get(i)+" : ");
						for (OffreWS o : listetouteslesOffres.get(i)) {
							System.out.println("Hôtel : "+o.getNomHotel()+", Adresse : "+o.getPaysHotel()+", "+o.getVilleHotel()+", "+
						o.getRueHotel()+", Nombre d'étoile : "+o.getNombreEtoile()+", Nombre de lits : "+o.getNombreLits()+", Prix : "+o.getPrix());							
						}
						System.out.println("--------------\n");
					}
				}
				
				
				System.out.println("Merci à bientôt !\n");
				System.exit(0);
				break;
				
			case "0":
				sc.close();
				System.out.println("Merci à bientôt !");
				System.exit(0);
				return;
			
			default:
				System.err.println("Mauvaise input /!\\ Veuillez entrer le nombre 1 ou 0");
				break;
			}

		}
	}
}


